module N_star : sig
  type one
  type 'a s

  type _ t =
    | One : one t
    | Succ : 'a t -> 'a s t

  val to_int : 'a t -> int
end = struct
  type one
  type 'a s

  type _ t =
    | One : one t
    | Succ : 'a t -> 'a s t

  let rec to_int : type a. a t -> int = function
    | One -> 1
    | Succ n -> succ (to_int n)
end

type _ f =
  | Ret : 'a t -> 'a t f
  | App : 'a t * 'b f -> ('a * 'b) f

and _ t =
  | Int : 'a N_star.t -> (int * 'a N_star.t) t
  | Array : int * 'a t -> 'a value list t
  | Pointer : 'a t -> 'a t t
  | FunctionPointer : 'a f -> 'a f t

and 'a value = 'a t * string

type 'a either =
  | Value of 'a value
  | Type of 'a t

let rec to_string : type a. a t -> string = fun data ->
  let rec aux : type a. string -> a f -> string = fun acc -> function
    | Ret t -> to_string t ^ " @(" ^ acc ^ ") *"
    | App (t, f) -> aux (acc ^ ", " ^ to_string t) f (* FALSE ! Also before *)
  in
  match data with
    | Int i -> "i" ^ string_of_int (N_star.to_int i)
    | Array (i, t) ->
        "[" ^ string_of_int i ^ " x " ^ to_string t ^ "]"
    | Pointer t -> to_string t ^ "*"
    | FunctionPointer f -> aux "" f

let value_to_string (t, value) = to_string t ^ " " ^ value

type one = N_star.one
type 'a seven_plus = 'a N_star.s N_star.s N_star.s N_star.s N_star.s N_star.s N_star.s
type 'a eight_plus = 'a N_star.s seven_plus
type eight = N_star.one seven_plus
type sixteen = eight eight_plus
type thirty_two = sixteen eight_plus eight_plus
type sixty_four = thirty_two eight_plus eight_plus eight_plus eight_plus

let one = N_star.One
let seven_plus x = N_star.(Succ (Succ (Succ (Succ (Succ (Succ (Succ x)))))))
let eight_plus x = seven_plus (N_star.Succ x)
let eight = seven_plus one
let sixteen = eight_plus eight
let thirty_two = eight_plus (eight_plus sixteen)
let sixty_four = eight_plus (eight_plus (eight_plus (eight_plus thirty_two)))

let int x value = (Int x, string_of_int value)
let int1 = int one
let int8 = int eight
let int16 = int sixteen
let int32 = int thirty_two
let int64 = int sixty_four
let array n t value =
  (Array (n, t), "[" ^ String.concat ", " (List.map value_to_string value) ^ "]")
let pointer t value = (Pointer t, to_string value)

let string n value = (Array (n, Int eight), "c\"" ^ String.escaped value ^ "\\00\"")
