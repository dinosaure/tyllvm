module N_star : sig
  type one
  type 'a s

  type _ t =
    | One : one t
    | Succ : 'a t -> 'a s t

  val to_int : 'a t -> int
end

type _ f =
  | Ret : 'a t -> 'a t f
  | App : 'a t * 'b f -> ('a * 'b) f

and _ t =
  | Int : 'a N_star.t -> (int * 'a N_star.t) t
  | Array : int * 'a t -> 'a value list t
  | Pointer : 'a t -> 'a t t
  | FunctionPointer : 'a f -> 'a f t

and 'a value = 'a t * string

type 'a either =
  | Value of 'a value
  | Type of 'a t

val to_string : 'a t -> string
val value_to_string : 'a value -> string

type one = N_star.one
type 'a seven_plus = 'a N_star.s N_star.s N_star.s N_star.s N_star.s N_star.s N_star.s
type 'a eight_plus = 'a N_star.s seven_plus
type eight = N_star.one seven_plus
type sixteen = eight eight_plus
type thirty_two = sixteen eight_plus eight_plus
type sixty_four = thirty_two eight_plus eight_plus eight_plus eight_plus

val eight : eight N_star.t
val seven_plus : 'a N_star.t -> 'a seven_plus N_star.t
val eight_plus : 'a N_star.t -> 'a eight_plus N_star.t
val sixteen : sixteen N_star.t
val thirty_two : thirty_two N_star.t
val sixty_four : sixty_four N_star.t

val int : 'a N_star.t -> int -> (int * 'a N_star.t) value
val int1 : int -> (int * one N_star.t) value
val int8 : int -> (int * eight N_star.t) value
val int16 : int -> (int * sixteen N_star.t) value
val int32 : int -> (int * thirty_two N_star.t) value
val int64 : int -> (int * sixty_four N_star.t) value
val array : int -> 'a t -> 'a value list -> 'a value list value
val pointer : 'a t -> 'a t -> 'a t value
val string : int -> string -> (int * eight N_star.t) value list value
