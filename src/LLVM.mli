type link_type = [
  | `Private
  | `Linker_private
  | `Linker_private_weak
  | `Internal
  | `Available_externally (* only allowed on definitions, not declarations *)
  | `Linkonce
  | `Weak
  | `Common
  | `Appending
  | `Extern_weak
  | `Linkonce_odr
  | `Weak_odr
  | `Linkonce_odr_auto_hide
  | `External
  | `Dllimport
  | `Dllexport
  ]

type address =
  | Addrspace of int
  | Unnamed_addr
  | None_addr

type thread_local =
  | Localdynamic
  | Initialexec
  | Localexec

type calling_convention =
  | CCC
  | FastCC
  | ColdCC
  | CC of int

type visibility =
  | Default
  | Hidden
  | Protected

type function_attribute =
  | Alignstack of int
  | Alwaysinline
  | Cold
  | Nonlazybind
  | Inlinehint
  | Naked
  | Nobuiltin
  | Noduplicate
  | Noimplicitefloat
  | Noinline
  | Noredzone
  | Noreturn
  | Nounwind
  | Optsize
  | Readnone
  | Readonly
  | Returns_twice
  | Sanitize_address
  | Sanitize_memory
  | Sanitize_thread
  | Ssp
  | Sspreq
  | Sspstrong
  | Uwtable

type parameter_attribute = [
  | `Zeroext
  | `Signext
  | `Inreg
  | `Byval
  | `Sret
  | `Noalias
  | `Nocapture
  | `Nest
  | `Returned
  ]

type ('a, 'b) expr =
  | Call of (bool * calling_convention option *
    [`Zeroext | `Signext | `Inreg ] option * 'a LLVM_types.t *
    ('a, 'b) expr * (('a, 'b) expr list))
  | Variable_global of string
  | Variable_local of string
  | Assign of (string * ('a, 'b) expr)
  | GetElementPtr of (bool * 'a LLVM_types.t * ('a, 'b) expr *
    'b LLVM_types.value list)

type ('a, 'b) instr =
  | Ret of 'a LLVM_types.value
  | Expr of ('a, 'b) expr

type ('a, 'b, 'c, 'd) context_module =
  | Global of
    (string * thread_local option *
    address * link_type option * bool *
    'a LLVM_types.either * string option * int option)
  | Declaration of
    (string * 'b LLVM_types.f * [`External | `Dllimport | `Extern_weak] option)
  | Definition of
    ((string * 'b LLVM_types.f) * ('c, 'd) instr list)

(*
 *  Global:
 *    - identifier
 *    - may be defined as thread_local
 *    - can be marked with unnamed_address | may be declared to reside in a
 *      target-specific numbered address space
 *    - have one of types of linkage
 *    - may be defined as a global constant
 *    - may optionally be initialized (not present)
 *    - may have an explicit section to be placed in
 *    - may have an optional explicit alignment specified (power of 2)
 *)

(*
 * Function declaration:
 *    - it is illegal for a function declaration to have any linkage type
 *      other than external, dllimport or extern_weak
 *    - function attribute
 *)

val call:
  ?tail: bool ->
  ?cconv: calling_convention option ->
  ?ret_attr: [< `Zeroext | `Signext | `Inreg ] option ->
  'a LLVM_types.t -> ('a, 'b) expr -> ('a, 'b) expr list -> ('a, 'b) expr

val getelementptr:
  ?inbounds: bool ->
  'a LLVM_types.t -> ('a, 'b) expr -> ('b LLVM_types.value) list ->
  ('a, 'b) expr

val new_global:
  ?thread_local:thread_local option ->
  ?address: address ->
  ?link_type: link_type option ->
  ?constant: bool ->
  ?section: string option ->
  ?alignment: int option ->
  string -> 'a LLVM_types.either -> ('a, 'b, 'c, 'd) context_module

val new_declaration:
  ?link_type: [< `External | `Dllimport | `Extern_weak ] option ->
  string -> 'b LLVM_types.f -> ('a, 'b, 'c, 'd) context_module

val new_definition:
  string -> 'b LLVM_types.f -> ('c, 'd) instr list ->
  ('a, 'b, 'c, 'd) context_module

val print_context_module: ('a, 'b, 'c, 'd)  context_module -> unit
